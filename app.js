const app = require('express')();
const { PORT } = require('./config/general.config')


app.get('/', (req, res) => {
  res.send('hello world, this is test second times ')
})

app.listen(PORT, () => {
  console.log('App running on port:', PORT)
})